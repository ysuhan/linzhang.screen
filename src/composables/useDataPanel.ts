export const setMoveModel = () => {
  const panel = document.querySelector(".panel-model") as HTMLElement;

  let isDragging = false;
  let offsetX = 0;
  let offsetY = 0;

  if (panel) {
    panel.addEventListener("mousedown", (event: MouseEvent) => {
      isDragging = true;
      offsetX = event.clientX - panel.getBoundingClientRect().left;
      offsetY = event.clientY - panel.getBoundingClientRect().top;
      panel.style.cursor = "grabbing";
    });

    document.addEventListener("mousemove", (event: MouseEvent) => {
      if (isDragging) {
        const mouseX = event.clientX;
        const mouseY = event.clientY;
        const panelWidth = panel.offsetWidth;
        const panelHeight = panel.offsetHeight;
        const viewportWidth = window.innerWidth;
        const viewportHeight = window.innerHeight;

        // 计算新的位置
        let newLeft = mouseX - offsetX;
        let newTop = mouseY - offsetY;

        // 限制到屏幕边界
        if (newLeft < 0) {
          newLeft = 0;
        } else if (newLeft + panelWidth > viewportWidth) {
          newLeft = viewportWidth - panelWidth;
        }

        if (newTop < 0) {
          newTop = 0;
        } else if (newTop + panelHeight > viewportHeight) {
          newTop = viewportHeight - panelHeight;
        }

        // 设置新的位置
        panel.style.left = `${newLeft}px`;
        panel.style.top = `${newTop}px`;
      }
    });

    document.addEventListener("mouseup", () => {
      isDragging = false;
      panel.style.cursor = "move";
    });
  }
};
