import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  let serverConfig = {};
  if (mode === "development") {
    serverConfig = {
      proxy: {
        "/java-server": {
          target: "http://218.94.64.150:20007",
          changeOrigin: true,
          rewrite: (path) => {
            return path.replace(/^\/java-server/, "");
          },
        },
      },
    };
  }

  return {
    base: "/screen/",
    plugins: [vue()],
    server: serverConfig,
    build: {
      outDir: "./screen",
    },
  };
});
